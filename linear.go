package linear

type Interval struct {
	Left  float64
	Right float64
	Value float64
}

func Clip(clipLeft, clipRight, left, right, value float64) float64 {

	if clipLeft >= clipRight {
		return 0.0
	}

	if left >= right {
		return 0.0
	}

	if left > clipRight {
		return 0.0
	}

	if right < clipLeft {
		return 0.0
	}

	origValue := value
	origSize := right - left

	if clipLeft > left {
		sizeToClip := clipLeft - left
		amountToClip := origValue * sizeToClip / origSize
		value -= amountToClip
	}

	if clipRight < right {
		sizeToClip := right - clipRight
		amountToClip := origValue * sizeToClip / origSize
		value -= amountToClip
	}

	return value
}

func ClipAndSum(clipLeft, clipRight float64, intervals []Interval) float64 {

	var result float64

	for _, interval := range intervals {
		result += Clip(clipLeft, clipRight, interval.Left, interval.Right, interval.Value)
	}

	return result
}
