package linear

import (
	"testing"
)

func TestClipWithInvalidClipInterval(t *testing.T) {
	result := Clip(10.0, -10.0, 0.0, 10.0, 10.0)
	if result != 0.0 {
		t.Errorf("Returned %f instead of 0.0", result)
	}
}

func TestClipWithInvalidInterval(t *testing.T) {
	result := Clip(0.0, 10.0, 10.0, 0.0, 10.0)
	if result != 0.0 {
		t.Errorf("Returned %f instead of 0.0", result)
	}
}

func TestClipWithZeroInterval(t *testing.T) {
	result := Clip(0.0, 0.0, 10.0, 0.0, 10.0)
	if result != 0.0 {
		t.Errorf("Returned %f instead of 0.0", result)
	}
}

func TestClipWithExactInterval(t *testing.T) {
	result := Clip(0.0, 10.0, 0.0, 10.0, 1234.0)
	if result != 1234.0 {
		t.Errorf("Returned %f instead of original value", result)
	}
}

func TestClipWithBiggerInterval(t *testing.T) {
	result := Clip(-10.0, 20.0, 0.0, 10.0, 1234.0)
	if result != 1234.0 {
		t.Errorf("Returned %f instead of original value", result)
	}
}

func TestClipWithLeftClipping(t *testing.T) {
	result := Clip(5.0, 10.0, 0.0, 10.0, 1234.0)
	if result != 1234.0 / 2.0 {
		t.Errorf("Returned %f instead of clipped value", result)
	}
}

func TestClipWithRightClipping(t *testing.T) {
	result := Clip(0.0, 5.0, 0.0, 10.0, 1234.0)
	if result != 1234.0 / 2.0 {
		t.Errorf("Returned %f instead of clipped value", result)
	}
}

func TestClipWithFullClipping(t *testing.T) {
	result := Clip(2.5, 7.5, 0.0, 10.0, 1234.0)
	if result != 1234.0 / 2.0 {
		t.Errorf("Returned %f instead of clipped value", result)
	}
}

func TestClipOutsideLeft(t *testing.T) {
	result := Clip(-10.0, -5.0, 0.0, 10.0, 1234.0)
	if result != 0.0 {
		t.Errorf("Returned %f instead of 0.0", result)
	}
}

func TestClipOutsideRight(t *testing.T) {
	result := Clip(15.0, 20.0, 0.0, 10.0, 1234.0)
	if result != 0.0 {
		t.Errorf("Returned %f instead of 0.0", result)
	}
}

func TestClipStraddlingLeft(t *testing.T) {
	result := Clip(-5.0, 5.0, 0.0, 10.0, 1234.0)
	if result != 1234.0 / 2 {
		t.Errorf("Returned %f instead of half value", result)
	}
}

func TestClipStraddlingRight(t *testing.T) {
	result := Clip(5.0, 15.0, 0.0, 10.0, 1234.0)
	if result != 1234.0 / 2 {
		t.Errorf("Returned %f instead of half value", result)
	}
}
